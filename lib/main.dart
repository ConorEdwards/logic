import 'package:pl/Proof.dart';
import 'package:pl/Rule.dart';
import 'package:pl/Sentence.dart';
import 'package:pl/Sequents.dart';

void main(List<String> args) {

  Atom P = new Atom('P');
  Atom Q = new Atom('Q');
  Atom R = new Atom('R');

  Proof proof = new Proof();

  proof.newLine({1}, ~(P & (Q & R)), Assumption(), []);
  proof.newLine({2}, R & P, Assumption(), []);
  proof.newLine({2}, P, AndElimination(), [2]);
  proof.newLine({2}, ~~P, DoubleNegation(), [3]);
  proof.newLine({1}, ~P | ~(Q & R), DeMorgansLaws(), [1]);
  proof.newLine({1,2}, ~(Q & R), DisjunctiveSyllogism(), [4,5]);
  proof.newLine({2}, R, AndElimination(), [2]);
  proof.newLine({2}, ~~R, DoubleNegation(), [7]);
  proof.newLine({1,2}, ~Q | ~R, DeMorgansLaws(), [6]);
  proof.newLine({1,2}, ~Q, DisjunctiveSyllogism(), [8,9]);
  proof.newLine({1}, (R & P) > ~Q, ConditionalProof(), [2,10]);
  proof.newLine({}, P | ~P, TheoremIntroduction(LawExcludedMiddle), []);

  print(proof);

}