
import "package:pl/Rule.dart";
import "package:pl/Sentence.dart";

final class Proof {

  List<Line> lines;

  Proof() : lines = new List<Line>.empty(growable: true);

  Line operator [] (int lineNumber) => lines[lineNumber - 1];

  String toString() => lines.join('\n');

  void newLine(Set<int> assumptions, WFF sentence, Rule rule, List<int> citations) {
    Line line = new Line(assumptions, lines.length + 1, sentence, rule, citations.map((int cit) => this[cit]).toList());
    lines.add(line);
    line.validate();
  }

}

final class Line {

  Set<int> assumptions;
  int number;
  WFF sentence;
  Rule rule;
  List<Line> citations;

  Line(this.assumptions, this.number, this.sentence, this.rule, this.citations);

  void validate() {
    rule.validate(citations, this);
  }

  String toString() => '$formatAssumptions$formatNumber$formatSentence$formatRule$formatCitations$formatReference';

  String get formatAssumptions => assumptions.join(',').padRight(15);
  String get formatNumber => number.toString().padRight(4);
  String get formatSentence => sentence.toString().padRight(40);
  String get formatRule => rule.toString().padRight(4);
  String get formatCitations => citations.map((Line cit) => cit.number).join(',').padRight(15);
  String get formatReference => rule.reference.padRight(20);

}

final class Sequent {

  final String name;
  final Set<Application> applications;

  Sequent(this.name, this.applications);

}

final class Theorem extends Sequent {

  Theorem(String name, Set<Cookie> outputs) : super(name, {for (Cookie output in outputs) (inputs: [], output: output)});

}
