
typedef WFF = Sentence<Atom>;
typedef Cookie = Sentence<Shape>;

sealed class Sentence<T extends Unit<T>> {

  Sentence();

  bool equals(Sentence<T> other);
  bool matches(Sentence other);

  Negation<T> operator ~ () => new Negation(this);
  Disjunction<T> operator | (Sentence<T> other) => new Disjunction(this, other);
  Conjunction<T> operator & (Sentence<T> other) => new Conjunction(this, other);
  Conditional<T> operator > (Sentence<T> other) => new Conditional(this, other);
  bool operator == (Object other) => other is Sentence<T> && equals(other);
  bool operator % (Sentence other) => matches(other);

  int get hashCode => "$runtimeType $toString()".hashCode;

}

sealed class Unit<T extends Unit<T>> extends Sentence<T> {

  final String symbol;

  Unit(this.symbol);

}

final class Atom extends Unit<Atom> {

  Atom(super.symbol);

  bool equals(Sentence<Atom> other) => other is Atom && symbol == other.symbol;
  bool matches(Sentence other) {

    if (other is! Unit) return false;
    switch (other) {

      case Atom():
        return this == other;
      
      case Shape():
        return false;

    }

  }

  String toString() => symbol;

}

final class Shape extends Unit<Shape> {

  Sentence? sentence;

  Shape(super.symbol) : sentence = null;

  bool equals(Sentence<Shape> other) => other is Shape && symbol == other.symbol;
  bool matches(Sentence other) {

    if (other is! Sentence<Atom>) return false;

    if (sentence == null) {
      sentence = other;
      return true;
    }
    return sentence == other;

  }

  String toString() => '$symbol[${sentence ?? "_"}]';

}

final class Negation<T extends Unit<T>> extends Sentence<T> {

  Sentence<T> negand;

  Negation(this.negand);

  bool equals(Sentence<T> other) => other is Negation<T> && negand == other.negand;
  bool matches(Sentence other) => other is Negation && negand % other.negand;

  String toString() => '~$negand';

}

final class Disjunction<T extends Unit<T>> extends Sentence<T> {

  Sentence<T> leftDisjunct, rightDisjunct;

  Disjunction(this.leftDisjunct, this.rightDisjunct);

  bool equals(Sentence<T> other) => other is Disjunction<T> && leftDisjunct == other.leftDisjunct && rightDisjunct == other.rightDisjunct;
  bool matches(Sentence other) => other is Disjunction && leftDisjunct % other.leftDisjunct && rightDisjunct % other.rightDisjunct;

  String toString() => '($leftDisjunct | $rightDisjunct)';

}

final class Conjunction<T extends Unit<T>> extends Sentence<T> {

  Sentence<T> leftConjunct, rightConjunct;

  Conjunction(this.leftConjunct, this.rightConjunct);

  bool equals(Sentence<T> other) => other is Conjunction<T> && leftConjunct == other.leftConjunct && rightConjunct == other.rightConjunct;
  bool matches(Sentence other) => other is Conjunction && leftConjunct % other.leftConjunct && rightConjunct % other.rightConjunct;

  String toString() => '($leftConjunct & $rightConjunct)';

}

final class Conditional<T extends Unit<T>> extends Sentence<T> {

  Sentence<T> antecedent, consequent;

  Conditional(this.antecedent, this.consequent);

  bool equals(Sentence<T> other) => other is Conditional<T> && antecedent == other.antecedent && consequent == other.consequent;
  bool matches(Sentence other) => other is Conditional && antecedent % other.antecedent && consequent % other.consequent;

  String toString() => '($antecedent -> $consequent)';

}
