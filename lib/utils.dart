
const bool DEBUG = false;

void printbug(Object? message) {
  if (DEBUG) print('DEBUG: $message');
}

extension SetEquality<T> on Set<T> {

  bool sameAs(Set<T> other) => this.difference(other).isEmpty && other.difference(this).isEmpty;

}