
import 'package:pl/Proof.dart';
import 'package:pl/Rule.dart';

Theorem LawExcludedMiddle = new Theorem(
  'Law of Excluded Middle',
  {
    A | ~A,
    ~A | A,
  }
);