
import 'package:pl/Proof.dart';
import 'package:pl/Sentence.dart';
import 'package:pl/utils.dart';

typedef Application = ({List<Cookie> inputs, Cookie output}); // a list of input cookie cutters and an output cookie cutter
typedef AssumptionSet = ({int citation, int? exclude}); // index of line to pool assumptions from and optional assumption to not include, index starts at 1 because 0 is the currrent line
typedef RuleRequirement = ({int citation, Rule rule});

Shape A = new Shape('__A__');
Shape B = new Shape('__B__');
Shape C = new Shape('__C__');

sealed class Rule {

  late final String name;
  late final Set<Application> applications;
  late final Set<AssumptionSet> assumptions;
  late final Set<RuleRequirement> requirements;
  String get reference => '';

  static void clear() {
    printbug('Cleared rule shapes');
    A = new Shape('__A__');
    B = new Shape('__B__');
    C = new Shape('__C__');
  }

  void validate(List<Line> inputs, Line output) {

    printbug('Analyzing rule $name with inputs:');
    printbug(inputs.join('\n'));
    printbug('And output:');
    printbug(output);

    // Validate number of input lines

    int expectedNumInputs = applications.first.inputs.length;
    if (expectedNumInputs != inputs.length)
      throw 'Wrong number of input lines';

    // Find a valid application of the rule

    Application? application = null;

    for (Application app in applications) {

      printbug('Checking new rule application');

      clear();

      printbug('Checking input lines');

      // Validate input lines

      for (int i = 0 ; i < expectedNumInputs ; i++) {
        
        printbug('Checking input line ${i + 1}');
        printbug('Input line\n${inputs[i]}');
        printbug('Cookie cutter line\n${app.inputs[i]}');
        
        if (!(app.inputs[i] % inputs[i].sentence)) {
          
          printbug('Lines did not match');
          printbug('Input lines not valid');
          
          continue;
        }

        printbug('Lines matched');
      
      }

      printbug('Input lines valid');
      printbug('Checking output line');
      printbug('Output line\n$output');
      printbug('Cookie cutter line\n${app.output}');

      // Validate output line

      if (app.output % output.sentence) {

        printbug('Lines matched');
        
        application = app;
        break;
      }

      printbug('Lines did not match');

    }

    if (application == null)
      throw 'Rule does not apply';

    // Get expected assumptions
    
    Set<int> expectedAssumptions = new Set<int>();
    for (AssumptionSet set in assumptions) {

      Set<int> toAdd;
      if (set.citation != 0)
        toAdd = inputs[set.citation - 1].assumptions;
      else
        toAdd = output.assumptions;

      if (set.exclude case int exclude) {
        Line toRemove = exclude == 0 ? output : inputs[exclude - 1];
        toAdd = toAdd.difference({toRemove.number});
      }

      expectedAssumptions = expectedAssumptions.union(toAdd);

    }

    printbug('Expected assumptions: $expectedAssumptions');
    printbug('Gotten assumptions: ${output.assumptions}');

    // Validate assumptions

    if (!output.assumptions.sameAs(expectedAssumptions))
      throw 'Incorrect assumption column';

    // Validate citation requirements

    for (RuleRequirement req in requirements) {

      printbug('Testing requirement that citation ${req.citation} uses rule ${req.rule}');
      printbug('Cited line is line ${inputs[req.citation].number}:');
      printbug(inputs[req.citation]);

      if (inputs[req.citation - 1].rule != req.rule) {
        throw 'Wrong rule cited';
      }

    }

    // Line is valid
    
    printbug('Line is valid');

  }

  String toString() => name;

  bool operator == (Object other) => runtimeType == other.runtimeType;
  int get hashCode => name.hashCode;

}

final class Assumption extends Rule {

  final String name = 'A';

  final Set<Application> applications = {
    (inputs: [], output: A),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 0, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class ModusPonens extends Rule {

  final String name = 'MPP';

  final Set<Application> applications = {
    (inputs: [A, A > B], output: B),
    (inputs: [A > B, A], output: B),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
    (citation: 2, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class ConditionalProof extends Rule {

  final String name = 'CP';

  final Set<Application> applications = {
    (inputs: [A, B], output: A > B),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 2, exclude: 1),
  };

  final Set<RuleRequirement> requirements = {
    (citation: 1, rule: Assumption()),
  };

}

final class DoubleNegation extends Rule {

  final String name = 'DN';

  final Set<Application> applications = {
    (inputs: [A], output: ~~A),
    (inputs: [~~A], output: A),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class ModusTollens extends Rule {

  final String name = 'MTT';

  final Set<Application> applications = {
    (inputs: [~B, A > B], output: ~A),
    (inputs: [A > B, ~B], output: ~A),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
    (citation: 2, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class AndElimination extends Rule {

  final String name = '&E';

  final Set<Application> applications = {
    (inputs: [A & B], output: A),
    (inputs: [A & B], output: B),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class AndIntroduction extends Rule {

  final String name = '&I';

  final Set<Application> applications = {
    (inputs: [A, B], output: A & B),
    (inputs: [A, B], output: B & A),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
    (citation: 2, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class OrIntroduction extends Rule {

  final String name = '|I';

  final Set<Application> applications = {
    (inputs: [A], output: A | B),
    (inputs: [A], output: B | A),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class OrElimination extends Rule {

  final String name = '|E';

  final Set<Application> applications = {
    (inputs: [A | B, A, C, B, C], output: C),
    (inputs: [A | B, B, C, A, C], output: C),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
    (citation: 3, exclude: 2),
    (citation: 5, exclude: 4),
  };

  final Set<RuleRequirement> requirements = {
    (citation: 2, rule: Assumption()),
    (citation: 4, rule: Assumption()),
  };

}

final class ReductioAdAbsurdum extends Rule {

  final String name = 'RAA';

  final Set<Application> applications = {
    (inputs: [A, B & ~C], output: ~A),
    (inputs: [A, ~B & C], output: ~A),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
  };

  final Set<RuleRequirement> requirements = {
    (citation: 1, rule: Assumption()),
  };

}

final class DisjunctiveSyllogism extends Rule {

  final String name = 'DS';

  final Set<Application> applications = {
    (inputs: [A | B, ~A], output: B),
    (inputs: [A | B, ~B], output: A),
    (inputs: [~A, A | B], output: B),
    (inputs: [~B, A | B], output: A),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
    (citation: 2, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class DeMorgansLaws extends Rule {

  final String name = 'DeM';

  final Set<Application> applications = {
    (inputs: [~A | ~B], output: ~(A & B)),
    (inputs: [~A & ~B], output: ~(A | B)),
    (inputs: [~(A | B)], output: ~A & ~B),
    (inputs: [~(A & B)], output: ~A | ~B),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class ExFalsoQuodLibet extends Rule {

  final String name = 'EFQ';

  final Set<Application> applications = {
    (inputs: [A & ~A], output: B),
    (inputs: [~A & A], output: B),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class DisjunctionConditionalInterchange extends Rule {

  final String name = 'DC';

  final Set<Application> applications = {
    (inputs: [A > B], output: ~A | B),
    (inputs: [~A | B], output: A > B),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class NegatedConditionalConjunctionInterchange extends Rule {

  final String name = 'NC';

  final Set<Application> applications = {
    (inputs: [~(A > B)], output: A & ~B),
    (inputs: [A & ~B], output: ~(A > B)),
  };

  final Set<AssumptionSet> assumptions = {
    (citation: 1, exclude: null),
  };

  final Set<RuleRequirement> requirements = {};

}

final class TheoremIntroduction extends Rule {

  final Theorem theorem;

  String get reference => theorem.name;

  TheoremIntroduction(this.theorem) {
    applications = theorem.applications;
  }

  final String name = 'TI';

  late final Set<Application> applications;

  final Set<AssumptionSet> assumptions = {};

  final Set<RuleRequirement> requirements = {};

}

final class SequentIntroduction extends Rule {

  final Sequent sequent;

  String get reference => sequent.name;

  SequentIntroduction(this.sequent) {
    applications = sequent.applications;
    assumptions = {
      for (int i = 1 ; i < sequent.applications.first.inputs.length + 1 ; i++) (citation: i, exclude: null)
    };
  }

  final String name = 'TI';

  late final Set<Application> applications;

  late final Set<AssumptionSet> assumptions;

  final Set<RuleRequirement> requirements = {};

}




// final class  extends Rule {

//   final String name = '';

//   final Set<Application> applications = {
//     (inputs: , output: ),
//   };

//   final Set<AssumptionSet> assumptions = {
//     (citation: , exclude: ),
//   };

//   final Set<RuleRequirement> requirements = {
//     (citation: , rule: ),
//   };

// }
